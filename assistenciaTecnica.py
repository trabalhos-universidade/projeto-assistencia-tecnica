#!/usr/bin/python

import gtk
import gtk.glade
import os

gladefile = gtk.glade.XML('assistenciatecnica.glade')

# ----------------------------------------
# widgets
# ----------------------------------------

# Ip campo e botao
#ip = gladefile.get_widget('ip')
#campo_ip = gladefile.get_widget('campo_ip')
#conectar = gladefile.get_widget('bConectar')

# Espaco onde mostra mensagem
#resultado_status = gladefile.get_widget('resultado_status')
#jogada_servidor = gladefile.get_widget('ljogadaServidor')
#jogada_cliente = gladefile.get_widget('ljogadacliente')


# Tipos de conexao
#radioServidor = gladefile.get_widget('radioServidor')
#radioCliente = gladefile.get_widget('radioCliente')
#conexao = gladefile.get_widget('conexao')



def on_cadastrar_tipo_servico1_button_release_event(*args):
   
   form = gtk.glade.XML('formcadastroservico.glade')
   form.signal_autoconnect(dic)

def on_cadastrar(*args):
   
   form1 = gtk.glade.XML('formcadastro.glade')
   form1.signal_autoconnect(dic)

dic = { 
          "gtk_main_quit" : gtk.main_quit,
          "gtk_widget_show" : on_cadastrar_tipo_servico1_button_release_event,
          "gtk_widget_show" : on_cadastrar
          
		  
      }

gladefile.signal_autoconnect(dic)

gtk.main()
